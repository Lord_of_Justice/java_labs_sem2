package Thread;

import java.util.concurrent.CountDownLatch;

public class Threads{

    public static void main(String[] args) {
        System.out.println("Main thread started! \n");

        Thread1 thread1 = new Thread1("Thread 1");
        Thread2 thread2 = new Thread2("Thread 2");
        Thread3 thread3 = new Thread3("Thread 3");
        Thread4 thread4 = new Thread4("Thread 4");
        Thread5 thread5 = new Thread5("Thread 5");

        // чекаємо на закінчення всіх потоків
        try {
            thread1.t.join();
            thread2.t.join();
            thread3.t.join();
            thread4.t.join();
            thread5.t.join();
        } catch (InterruptedException e) {
            System.out.println("Main thread was interrupted!");
        }
        System.out.println("\nMain thread finished!");
    }

    public static class Thread1 extends IThreads{
        public Thread1(String name) {
            t = new Thread(this, name);
            t.start();
        }

        @Override
        public void task_init() throws InterruptedException {
            t.sleep(2000);
            System.out.println(t.getName() + ": finish task_init");
            Latchs.latch_2.countDown();
            Latchs.latch_3.countDown();
            Latchs.latch_4.countDown();
            Latchs.latch_5.countDown();

            // чекаєм поки інші покоти зроблять init
            Latchs.latch_1.await();
        }

        @Override
        public void task() throws InterruptedException {
            t.sleep(1000);
            System.out.println(t.getName() + ": finish task");
            Latchs.latch_for_1_and_2.await();
        }

        @Override
        public void task_finalize() throws InterruptedException {
            t.sleep(1500);
            System.out.println(t.getName() + ": finish task_finalize");
            Latchs.latch_5_finish.countDown();
        }
    }

    public static class Thread2 extends IThreads{
        public Thread2(String name) {
            t = new Thread(this, name);
            t.start();
        }

        @Override
        public void task_init() throws InterruptedException {
            t.sleep(1000);
            System.out.println(t.getName() + ": finish task_init");
            Latchs.latch_1.countDown();
            Latchs.latch_3.countDown();
            Latchs.latch_4.countDown();
            Latchs.latch_5.countDown();

            // чекаєм поки інші покоти зроблять init
            Latchs.latch_2.await();
        }

        @Override
        public void task() throws InterruptedException {
            t.sleep(1500);
            System.out.println(t.getName() + ": finish task");
            Latchs.latch_for_1_and_2.await();
        }

        @Override
        public void task_finalize() throws InterruptedException {
            t.sleep(2000);
            System.out.println(t.getName() + ": finish task_finalize");
            Latchs.latch_5_finish.countDown();
        }
    }

    public static class Thread3 extends IThreads{
        public Thread3(String name) {
            t = new Thread(this, name);
            t.start();
        }

        @Override
        public void task_init() throws InterruptedException {
            t.sleep(5000);
            System.out.println(t.getName() + ": finish task_init");
            Latchs.latch_1.countDown();
            Latchs.latch_2.countDown();
            Latchs.latch_4.countDown();
            Latchs.latch_5.countDown();

            // чекаєм поки інші покоти зроблять init
            Latchs.latch_3.await();
        }

        @Override
        public void task() throws InterruptedException {
            t.sleep(500);
            System.out.println(t.getName() + ": finish task");
        }

        @Override
        public void task_finalize() throws InterruptedException {
            t.sleep(2500);
            System.out.println(t.getName() + ": finish task_finalize");
            Latchs.latch_for_1_and_2.countDown();
            Latchs.latch_5_finish.countDown();
        }
    }

    public static class Thread4 extends IThreads{
        public Thread4(String name) {
            t = new Thread(this, name);
            t.start();
        }

        @Override
        public void task_init() throws InterruptedException {
            t.sleep(500);
            System.out.println(t.getName() + ": finish task_init");
            Latchs.latch_1.countDown();
            Latchs.latch_2.countDown();
            Latchs.latch_3.countDown();
            Latchs.latch_5.countDown();

            // чекаєм поки інші покоти зроблять init
            Latchs.latch_4.await();
        }

        @Override
        public void task() throws InterruptedException {
            t.sleep(4000);
            System.out.println(t.getName() + ": finish task");
        }

        @Override
        public void task_finalize() throws InterruptedException {
            t.sleep(3000);
            System.out.println(t.getName() + ": finish task_finalize");
            Latchs.latch_for_1_and_2.countDown();
            Latchs.latch_5_finish.countDown();
        }
    }

    public static class Thread5 extends IThreads{
        public Thread5(String name) {
            t = new Thread(this, name);
            t.start();
        }

        @Override
        public void task_init() throws InterruptedException {
            t.sleep(2500);
            System.out.println(t.getName() + ": finish task_init");
            Latchs.latch_1.countDown();
            Latchs.latch_2.countDown();
            Latchs.latch_3.countDown();
            Latchs.latch_4.countDown();

            // чекаєм поки інші покоти зроблять init
            Latchs.latch_5.await();
        }

        @Override
        public void task() throws InterruptedException {
            t.sleep(750);
            System.out.println(t.getName() + ": finish task");
            Latchs.latch_5_finish.await();
        }

        @Override
        public void task_finalize() throws InterruptedException {
            t.sleep(500);
            System.out.println(t.getName() + ": finish task_finalize");
        }
    }
}


