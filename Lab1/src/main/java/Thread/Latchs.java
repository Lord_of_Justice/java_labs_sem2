package Thread;

import java.util.concurrent.CountDownLatch;

public class Latchs {
    public static CountDownLatch latch_1 = new CountDownLatch(4);
    public static CountDownLatch latch_2 = new CountDownLatch(4);
    public static CountDownLatch latch_3 = new CountDownLatch(4);
    public static CountDownLatch latch_4 = new CountDownLatch(4);
    public static CountDownLatch latch_5 = new CountDownLatch(4);

    public static CountDownLatch latch_for_1_and_2 = new CountDownLatch(2);
    public static CountDownLatch latch_5_finish = new CountDownLatch(4);
}
