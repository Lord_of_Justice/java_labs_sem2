package Thread;

public abstract class IThreads implements Runnable {
    Thread t;

    @Override
    public void run() {
        try {
            task_init();
            task();
            task_finalize();
        } catch (Exception e) {
            System.out.println(t.getName() + " was interrupted");
        }
    }

    public abstract void task_init() throws InterruptedException;
    public abstract void task() throws InterruptedException;
    public abstract void task_finalize() throws InterruptedException;
}
