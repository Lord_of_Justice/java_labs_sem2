package FileSystem;

import FileSystem.FsEntity.FsEntity;

public class TextFile extends FsEntity {

    String data;

    protected TextFile(String name, Directory parent, String data) {
        super(name, parent);
        this.data = data;
    }

    public String read() {
        return data;
    }
}
