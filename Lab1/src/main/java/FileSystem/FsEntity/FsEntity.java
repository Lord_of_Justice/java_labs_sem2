package FileSystem.FsEntity;

import FileSystem.Directory;

public class FsEntity implements IFsEntity {
    private Directory parent;
    private String name;

    protected FsEntity(String name) {
        if (name == null || name.length() == 0 || name.contains("/")) {
            throw new IllegalArgumentException("Name could not be null or zero-length and contain '/'");
        }
        this.name = name;
    }

    protected FsEntity(String name, Directory parent) {
        this(name);
        if (parent == null) {
            throw new IllegalArgumentException("Parent directory could not be null");
        }
        if (parent.containsChild(name)) {
            throw new IllegalArgumentException("Element with same name already exists");
        }
        this.parent = parent;
        parent.addChild(this);
    }

    public Directory getParent() {
        return parent;
    }

    public Directory getRoot() {
        if (parent == null) {
            return (Directory) this;
        }
        Directory current = parent;
        while (current.getParent() != null) {
            current = current.getParent();
        }
        return current;
    }

    public String getName() {
        return name;
    }

    @Override
    public void delete() {
        if (parent != null) {
            parent.removeChild(name);
        }
        parent = null;
    }

    private FsEntity getEntityByPath(String path) {
        String[] subFolders = path.split("/");
        FsEntity startPoint;
        int index = 0;
        if (path.startsWith("/")) {
            index++;
            startPoint = getRoot();
        } else {
            startPoint = this;
        }
        return getEntityByPathHelper(startPoint, subFolders, index);
    }

    private FsEntity getEntityByPathHelper(FsEntity current, String[] subFolders, int index) {
        if (!(current.getType() == FsEntityType.DIRECTORY)) {
            throw new IllegalArgumentException("Path not valid, expected directory");
        }
        if (index >= subFolders.length) {
            return current;
        }
        String nextPath = subFolders[index++];
        FsEntity nextEntity = ((Directory)current).getChild(nextPath);
        if (nextEntity == null) {
            throw new IllegalArgumentException("No such file or folder");
        }
        return getEntityByPathHelper(nextEntity, subFolders, index);
    }

    @Override
    public void move(Directory destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination could not be null");
        }
        if (parent != null) {
            parent.removeChild(name);
        }
        parent = destination;
        parent.addChild(this);
    }

    @Override
    public void move(String destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination could not be null");
        }
        FsEntity destinationEntity = getEntityByPath(destination);
        if (!(destinationEntity.getType() == FsEntityType.DIRECTORY)) {
            throw new IllegalArgumentException("Destination is not a directory");
        }
        move((Directory) destinationEntity);
    }

    @Override
    public FsEntityType getType() {
        return null;
    }
}
