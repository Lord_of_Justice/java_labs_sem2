package FileSystem.FsEntity;

import FileSystem.Directory;

public interface IFsEntity {
    void delete();
    void move(Directory destination);
    void move(String destination);
    FsEntityType getType();
}
