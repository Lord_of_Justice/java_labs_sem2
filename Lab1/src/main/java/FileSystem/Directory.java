package FileSystem;

import FileSystem.FsEntity.FsEntity;
import FileSystem.FsEntity.FsEntityType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Directory extends FsEntity {
    private static final int DIR_MAX_ELEMENTS = 10;
    private Map<String, FsEntity> children = new HashMap<>(DIR_MAX_ELEMENTS);

    public Directory(String name) {
        super(name);
    }

    public Directory(String name, Directory parent) {
        super(name, parent);
    }

    public static Directory create(String name, Directory parent) {
        if (parent == null) {
            return new Directory(name);
        }
        return new Directory(name, parent);
    }

    public boolean containsChild(String childName) {
        return children.containsKey(childName);
    }

    public int countChildren() {
        return children.size();
    }

    public FsEntity getChild(String childName) {
        return children.get(childName);
    }

    public void addChild(FsEntity child) {
        if (countChildren() >= DIR_MAX_ELEMENTS) {
            throw new IllegalCallerException("Too many elements in directory");
        }
        children.put(child.getName(), child);
    }

    public FsEntity removeChild(String childName) {
        return children.remove(childName);
    }

    public List<FsEntity> getChildren() {
        return new ArrayList<FsEntity>(children.values());
    }

    @Override
    public void delete() {
        if (countChildren() != 0) {
            throw new IllegalCallerException("Directory is not empty!");
        }
        super.delete();
    }

    @Override
    public void move(Directory destination) {
        if (getParent() == null) {
            throw new IllegalCallerException("It`s root directory. You can`t move it.");
        }
        super.move(destination);
    }

    @Override
    public void move(String destination) {
        if (getParent() == null) {
            throw new IllegalCallerException("It`s root directory. You can`t move it.");
        }
        super.move(destination);
    }

    @Override
    public FsEntityType getType() {
        return FsEntityType.DIRECTORY;
    }
}
