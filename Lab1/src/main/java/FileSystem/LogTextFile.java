package FileSystem;

import FileSystem.FsEntity.FsEntity;
import FileSystem.FsEntity.FsEntityType;

public class LogTextFile extends FsEntity {
    private StringBuffer data;

    public LogTextFile(String name, Directory parent, String data) {
        super(name, parent);
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        this.data = new StringBuffer(data);
    }

    public String read() {
        return data.toString();
    }

    public void append(String line) {
        if (line == null) {
            throw new IllegalArgumentException("Line should not be null");
        }
        data.append("\n" + line);
    }

    @Override
    public FsEntityType getType() {
        return FsEntityType.LOG_TEXT_FILE;
    }
}
