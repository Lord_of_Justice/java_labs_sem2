package FileSystem;

import FileSystem.FsEntity.FsEntity;
import FileSystem.FsEntity.FsEntityType;

public class BinaryFile extends FsEntity {
    private byte[] data;

    public BinaryFile(String name, Directory parent, byte[] data) {
        super(name, parent);
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        this.data = data;
    }

    public byte[] read() {
        return data.clone();
    }

    @Override
    public FsEntityType getType() {
        return FsEntityType.BINARY_FILE;
    }
}
