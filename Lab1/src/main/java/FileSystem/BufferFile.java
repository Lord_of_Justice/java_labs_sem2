package FileSystem;

import FileSystem.FsEntity.FsEntity;
import FileSystem.FsEntity.FsEntityType;

import java.util.LinkedList;
import java.util.Queue;

public class BufferFile<T> extends FsEntity {
    private final static int MAX_BUF_FILE_SIZE = 15;
    private Queue<T> queue;

    public BufferFile(String name, Directory parent) {
        super(name, parent);
        queue = new LinkedList<>();
    }

    public void push(T element) {
        if (element == null) {
            throw new IllegalArgumentException("Element could not be null");
        }
        if (queue.size() >= MAX_BUF_FILE_SIZE) {
            throw new IllegalCallerException("Too many elements in buffer");
        }
        // add to the last
        queue.offer(element);
    }

    public T consume() {
        // remove first
        return queue.remove();
    }

    @Override
    public FsEntityType getType() {
        return FsEntityType.BUFFER_FILE;
    }
}
