import FileSystem.FsEntity.FsEntity;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


import FileSystem.*;

public class FileSystemTest {

    Directory root;

    @BeforeEach
    public void init() {
        root = Directory.create("root", null);
        Directory subDir = new Directory("home", root);
        Directory anotherSubDir = new Directory("test", root);
        BinaryFile binary = new BinaryFile("binary", root, "Hello World!".getBytes());

        Directory subSubDir = new Directory("java", subDir);
        BufferFile bufferFile = new BufferFile("buffer", subDir);

        LogTextFile log = new LogTextFile("logger", anotherSubDir, "Note: you LOSE!");
    }

    @Test
    void Delete_SomeFsEntity_DeletedSuccessfully() {
        Directory subDir = (Directory)root.getChild("home");
        BufferFile file = (BufferFile)subDir.getChild("buffer");
        Directory subSubDir = (Directory)subDir.getChild("java");

        assertThrows(IllegalCallerException.class, () -> subDir.delete());
        assertDoesNotThrow(() -> subSubDir.delete());
        assertFalse(subDir.containsChild("java"));
        file.delete();
        assertFalse(subDir.containsChild("buffer"));
        assertDoesNotThrow(() -> subDir.delete());
        assertEquals(0, subDir.countChildren());
    }

    @Test
    void Move_DestinationInString_MovedSuccessfully() {
        BinaryFile binary = (BinaryFile)root.getChild("binary");
        Directory subDir = (Directory)root.getChild("home");
        Directory subSubDir = (Directory)subDir.getChild("java");

        binary.move("/home/java/");
        assertFalse(root.containsChild("binary"));
        assertEquals(binary, subSubDir.getChild("binary"));

        subSubDir.move("/");
        assertTrue(root.containsChild("java"));
    }

    @Test
    void Move_DictionaryDestination_MovedSuccessfully() {
        BinaryFile binary = (BinaryFile)root.getChild("binary");
        Directory subDir = (Directory)root.getChild("home");

        assertThrows(IllegalCallerException.class, () -> root.move(subDir));
        assertDoesNotThrow(() -> binary.move("/" + subDir.getName()));
        assertTrue(subDir.containsChild("binary"));
        assertFalse(root.containsChild("binary"));
    }

    // directory
    @Test
    void listDirectory_Root_ListChildren() {


        List listElements = new ArrayList<FsEntity>();
        Directory root2 = Directory.create("root", null);
        BinaryFile binary = new BinaryFile("binary", root2, "Hello World!".getBytes());
        Directory subDir = new Directory("home", root2);
        Directory anotherSubDir = new Directory("test", root2);

        listElements.add(subDir);
        listElements.add(anotherSubDir);
        listElements.add(binary);

        assertEquals(listElements.size(), root2.countChildren());
        List rootElements = root2.getChildren();
        for (int i = 0; i < rootElements.size(); i++) {
            FsEntity rootEl = (FsEntity)rootElements.get(i);
            assertTrue(listElements.contains(rootEl));
        }
    }

    // binary
    @Test
    void readBytes_File_SomeResult() {
        BinaryFile binary = (BinaryFile)root.getChild("binary");
        assertArrayEquals("Hello World!".getBytes(), binary.read());

        BinaryFile emptyFile = new BinaryFile("empty", root, "".getBytes());
        assertEquals(0, emptyFile.read().length);
    }

    // buffer file
    @Test
    void consumePush_File_SomeResult() {
        Directory subDir = (Directory)root.getChild("home");
        BufferFile buffer = (BufferFile)subDir.getChild("buffer");

        assertThrows(NoSuchElementException.class, () -> buffer.consume());
        buffer.push('Q');
        buffer.push('A');
        assertEquals('Q', buffer.consume());
        assertEquals('A', buffer.consume());
        assertThrows(NoSuchElementException.class, () -> buffer.consume());
    }

    @Test
    void consumePush_TooManyElements_ThrowsError() {
        Directory subDir = (Directory)root.getChild("home");
        BufferFile buffer = (BufferFile)subDir.getChild("buffer");

        for (int i = 0; i < 15; i++) {
            buffer.push((char) ('A' + i));
        }
        assertThrows(IllegalCallerException.class, () -> buffer.push('Z'));
    }

    // log file
    @Test
    void readAppend_File_GoodResult() {
        Directory subDir = (Directory)root.getChild("test");
        LogTextFile log = (LogTextFile)subDir.getChild("logger");

        assertEquals("Note: you LOSE!", log.read());
        log.append("You can`t win next time.");
        assertEquals("Note: you LOSE!\nYou can`t win next time.", log.read());
        log.append("");
        assertEquals("Note: you LOSE!\nYou can`t win next time.\n", log.read());
    }

}
