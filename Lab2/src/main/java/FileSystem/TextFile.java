package FileSystem;

import FileSystem.FsEntity.FsEntity;
import FileSystem.FsEntity.FsEntityType;

public class TextFile extends FsEntity {

    String data;

    public TextFile(String name, Directory parent, String data) {
        super(name, parent);
        this.data = data;
    }

    public String read() {
        return data;
    }

    @Override
    public FsEntityType getType() {
        return FsEntityType.TEXT_FILE;
    }
}
