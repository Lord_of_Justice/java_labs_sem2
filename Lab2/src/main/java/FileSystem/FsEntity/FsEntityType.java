package FileSystem.FsEntity;

public enum FsEntityType {
    DIRECTORY,
    BINARY_FILE,
    LOG_TEXT_FILE,
    BUFFER_FILE,
    TEXT_FILE
}
