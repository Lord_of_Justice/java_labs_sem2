package FileSystem.FsEntity;

import FileSystem.Directory;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FsEntity implements IFsEntity {
    private Directory parent;
    private String name;
    protected Lock mutex;

    protected FsEntity(String name) {
        if (name == null || name.length() == 0 || name.contains("/")) {
            throw new IllegalArgumentException("Name could not be null or zero-length and contain '/'");
        }
        this.name = name;
        mutex = new ReentrantLock();
    }

    protected FsEntity(String name, Directory parent) {
        this(name);
        if (parent == null) {
            throw new IllegalArgumentException("Parent directory could not be null");
        }
        if (parent.containsChild(name)) {
            throw new IllegalArgumentException("Element with same name already exists");
        }
        try{
            mutex.lock();
            this.parent = parent;
        } finally {
            mutex.unlock();
        }
        parent.addChild(this);
    }

    public Directory getParent() {
        return parent;
    }

    public Directory getRoot() {
        if (parent == null) {
            return (Directory) this;
        }
        Directory current = parent;
        while (current.getParent() != null) {
            current = current.getParent();
        }
        return current;
    }

    public String getName() {
        return name;
    }

    @Override
    public void delete() {
        try{
            mutex.lock();
            if (parent != null) {
                parent.removeChild(name);
            }
            parent = null;
        }
        finally {
            mutex.unlock();
        }

    }

    private FsEntity getEntityByPath(String path) {
        String[] subFolders = path.split("/");
        FsEntity startPoint;
        int index = 0;
        if (path.startsWith("/")) {
            index++;
            startPoint = getRoot();
        } else {
            startPoint = this;
        }
        return getEntityByPathHelper(startPoint, subFolders, index);
    }

    private FsEntity getEntityByPathHelper(FsEntity current, String[] subFolders, int index) {
        if (!(current.getType() == FsEntityType.DIRECTORY)) {
            throw new IllegalArgumentException("Path not valid, expected directory");
        }
        if (index >= subFolders.length) {
            return current;
        }
        String nextPath = subFolders[index++];
        FsEntity nextEntity = ((Directory)current).getChild(nextPath);
        if (nextEntity == null) {
            throw new IllegalArgumentException("No such file or folder");
        }
        return getEntityByPathHelper(nextEntity, subFolders, index);
    }

    @Override
    public void move(Directory destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination could not be null");
        }
        try {
            mutex.lock();
            if (parent != null) {
                parent.removeChild(name);
            }
            parent = destination;
            parent.addChild(this);
        } finally {
            mutex.unlock();
        }
    }

    @Override
    public void move(String destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination could not be null");
        }
        FsEntity destinationEntity = getEntityByPath(destination);
        if (!(destinationEntity.getType() == FsEntityType.DIRECTORY)) {
            throw new IllegalArgumentException("Destination is not a directory");
        }
        move((Directory) destinationEntity);
    }

    public String getPath() {
        if (parent == null) return "/" + getName();
        return ((FsEntity)parent).getPath() + "/" + getName();
    }

    @Override
    public FsEntityType getType() {
        return null;
    }
}
