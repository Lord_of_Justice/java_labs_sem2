package FileSystem.ForkJoinTasks;

import FileSystem.*;
import FileSystem.FsEntity.FsEntity;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class Search extends RecursiveTask<List<String>> {
    private Directory root;
    private String pattern;

    public Search(Directory node, String pattern) {
        this.root = node;
        this.pattern = pattern;
    }

    @Override
    protected List<String> compute() {
        List<String> path = new ArrayList<>();
        List<Search> subTasks = new LinkedList<>();

        for (FsEntity child : root.getChildren()) {
            if(child.getName().contains(pattern))
                path.add(child.getPath());
            if (child instanceof Directory) {
                Search task = new Search((Directory) child, pattern);
                task.fork();
                subTasks.add(task);
            }
        }

        for (Search task : subTasks) {
            path.addAll(task.join());
        }

        return path;
    }
}

