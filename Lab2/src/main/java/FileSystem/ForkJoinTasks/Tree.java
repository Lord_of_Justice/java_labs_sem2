package FileSystem.ForkJoinTasks;

import FileSystem.Directory;
import FileSystem.FsEntity.FsEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class Tree extends RecursiveTask<JsonNode>{
    private Directory node;
    private ObjectMapper mapper = new ObjectMapper();

    public Tree(Directory node) {
        this.node = node;
    }

    @Override
    protected JsonNode compute() {
        ArrayNode root = mapper.createArrayNode();
        List<Tree> subTasks = new LinkedList<>();

        for (FsEntity child : node.getChildren()) {
            JsonNode jNode = ToJson(child);
            if (child instanceof Directory) {
                Tree task = new Tree((Directory) child);
                task.fork();
                subTasks.add(task);
            }else {
                root.add(jNode);
            }
        }

        for (Tree task : subTasks) {
            JsonNode jsonNode = task.join();
            root.add(jsonNode);
        }

        ObjectNode result = mapper.createObjectNode();
        result.put(node.getName(), root);

        return result;
    }

    private JsonNode ToJson(FsEntity node){
        ObjectNode tmp = mapper.createObjectNode();
        tmp.put(node.getName(), node.getClass().getSimpleName());
        return tmp;
    }
}
