package FileSystem.ForkJoinTasks;

import FileSystem.*;
import FileSystem.FsEntity.FsEntity;
import FileSystem.FsEntity.FsEntityType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.RecursiveTask;


public class Archiving extends RecursiveTask<JsonNode> {
    private Directory node;
    private ObjectMapper mapper = new ObjectMapper();

    public Archiving(Directory node) {
        this.node = node;
    }

    @Override
    protected JsonNode compute() {
        ArrayNode root = mapper.createArrayNode();
        List<Archiving> subTasks = new LinkedList<>();

        for (FsEntity child : node.getChildren()) {
            JsonNode jNode = ToJsonFile(child);
            if (child.getType() == FsEntityType.DIRECTORY) {
                Archiving task = new Archiving((Directory) child);
                task.fork();
                subTasks.add(task);
            }else {
                root.add(jNode);
            }
        }

        for (Archiving task : subTasks) {
            JsonNode jsonNode = task.join();
            root.add(jsonNode);
        }

        ObjectNode result = mapper.createObjectNode();
        result.put(node.getName(), root);
        return result;
    }

    private JsonNode ToJsonFile(FsEntity node){
        ObjectNode tmp = mapper.createObjectNode();
        tmp.put(node.getName(), node.getClass().getSimpleName());
        FsEntityType type = node.getType();
        String data = "";
        switch (type){
            case BINARY_FILE:
                BinaryFile bin = (BinaryFile)node;
                data = new String(bin.read());
                break;
            case BUFFER_FILE:
                BufferFile bufferFile = (BufferFile)node;
                Queue lgQueue = null;
                try{
                    Field queue = bufferFile.getClass().getDeclaredField("queue");
                    queue.setAccessible(true);
                    lgQueue = (Queue) queue.get(bufferFile);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                int i = 1;
                while(lgQueue.size() != 0){
                    data += i + ") " + bufferFile.consume().toString() + ";\n";
                    i++;
                }
                break;
            case LOG_TEXT_FILE:
                LogTextFile lg = (LogTextFile) node;
                data = lg.read();
                break;
            case TEXT_FILE:
                TextFile text = (TextFile)node;
                data = text.read();
                break;
        }
        tmp.put("data", data);
        return tmp;
    }
}
