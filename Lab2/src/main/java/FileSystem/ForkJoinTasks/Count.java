package FileSystem.ForkJoinTasks;

import FileSystem.Directory;
import FileSystem.FsEntity.FsEntity;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class Count extends RecursiveTask<Long> {
    private Directory root;

    public Count(Directory node) {
        this.root = node;
    }

    @Override
    protected Long compute() {
        long sum = root.countChildren();
        List<Count> subTasks = new LinkedList<>();

        for(FsEntity child : root.getChildren()) {
            if (child instanceof Directory) {
                Count task = new Count((Directory)child);
                task.fork();
                subTasks.add(task);
            }
        }

        for(Count task : subTasks) {
            sum += task.join();
        }

        return sum;
    }
}
