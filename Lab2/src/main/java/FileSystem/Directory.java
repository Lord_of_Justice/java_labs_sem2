package FileSystem;

import FileSystem.ForkJoinTasks.*;
import FileSystem.FsEntity.FsEntity;
import FileSystem.FsEntity.FsEntityType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

public class Directory extends FsEntity {
    private static final int DIR_MAX_ELEMENTS = 1000;
    private Map<String, FsEntity> children = new HashMap<>(DIR_MAX_ELEMENTS);

    public Directory(String name) {
        super(name);
    }

    public Directory(String name, Directory parent) {
        super(name, parent);
    }

    public static Directory create(String name, Directory parent) {
        if (parent == null) {
            return new Directory(name);
        }
        return new Directory(name, parent);
    }

    public boolean containsChild(String childName) {
        return children.containsKey(childName);
    }

    public int countChildren() {
        return children.size();
    }

    public FsEntity getChild(String childName) {
        return children.get(childName);
    }

    public void addChild(FsEntity child) {
        try{
            mutex.lock();
            if (countChildren() >= DIR_MAX_ELEMENTS) {
                throw new IllegalCallerException("Too many elements in directory");
            }
            children.put(child.getName(), child);
        } finally {
            mutex.unlock();
        }

    }

    public FsEntity removeChild(String childName) {
        try{
            mutex.lock();
            return children.remove(childName);
        } finally {
            mutex.unlock();
        }
    }

    public List<FsEntity> getChildren() {
        return new ArrayList<FsEntity>(children.values());
    }

    @Override
    public void delete() {
        if (countChildren() != 0) {
            throw new IllegalCallerException("Directory is not empty!");
        }
        super.delete();
    }

    @Override
    public void move(Directory destination) {
        if (getParent() == null) {
            throw new IllegalCallerException("It`s root directory. You can`t move it.");
        }
        super.move(destination);
    }

    @Override
    public void move(String destination) {
        if (getParent() == null) {
            throw new IllegalCallerException("It`s root directory. You can`t move it.");
        }
        super.move(destination);
    }

    @Override
    public FsEntityType getType() {
        return FsEntityType.DIRECTORY;
    }

    public List<String> search(String pattern){
        return  new ForkJoinPool().invoke(new Search(this, pattern));
    }

    public Long count(boolean recursive){
        if(recursive){
            return new ForkJoinPool().invoke(new Count(this));
        }else{
            return (long) countChildren();
        }
    }

    public String archiving(){
        ObjectMapper mapper = new ObjectMapper();
        String result = "";
        try{
            result = mapper.writerWithDefaultPrettyPrinter()
                    .writeValueAsString(new ForkJoinPool().invoke(new Archiving(this)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String tree() {
        ObjectMapper mapper = new ObjectMapper();
        String result = "";
        try{
            result = mapper.writerWithDefaultPrettyPrinter()
                    .writeValueAsString(new ForkJoinPool().invoke(new Tree(this)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }
}
