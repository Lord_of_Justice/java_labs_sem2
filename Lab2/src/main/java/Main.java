import FileSystem.BinaryFile;
import FileSystem.BufferFile;
import FileSystem.Directory;
import FileSystem.FsEntity.FsEntity;
import com.fasterxml.jackson.core.JsonProcessingException;

public class Main {
    public static void main(String[] args) {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data", root);
        BufferFile bufferFile  = new BufferFile("bufferFile", dataDir);
        bufferFile.push("Try");
        bufferFile.push("To");
        bufferFile.push("Win");
        BinaryFile binFile = new BinaryFile("binFile", dataDir, "Hello World!".getBytes());

        String tree = root.tree();
        System.out.println(tree);

        String archiving = root.archiving();
        System.out.println(archiving);
    }
}
