package AuxiliaryMethods;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Controller {
    ExecutorService workers;

    public Controller(int amountOfWorkerThreads) {
        workers = Executors.newFixedThreadPool(amountOfWorkerThreads);
    }

    public <T>Future<T> submitFSAction (IFsAction<T> action) {
        try {
            return workers.submit(action::execute);
        } catch (Exception e){
            System.out.println(e.getMessage());
            throw e;
        }
    }

    public void stop(){
        workers.shutdown();
    }
}
