package AuxiliaryMethods;

import AuxiliaryMethods.utils.*;
import FileSystem.*;
import FileSystem.FsEntity.*;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Generator {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        Directory root = new Directory("Home");
        Controller controller = new Controller(5);

        int min = 1;
        int max = 100;
        String data = "Hello world!!!";

        for(int i = 0; i < max; i++){
            Random random = new Random();
            int n = random.nextInt(max - min + 1);
            Directory dir = (Directory)createEntity(controller, root, FsEntityType.DIRECTORY, "Dir" + i, "");
            if(i % 3 == 0){
                BinaryFile bf = (BinaryFile)createEntity(controller, dir, FsEntityType.BINARY_FILE, "Binary" + i, data + i);
                if(n == i)
                    controller.submitFSAction(new Delete(bf));
            }
            if(i % 4 == 0){
                Directory subDir = (Directory)createEntity(controller, dir, FsEntityType.DIRECTORY, "SubDir" + i, data + i);
                for(int j = 0; j < 3; j++){
                    createEntity(controller, subDir, FsEntityType.BUFFER_FILE, "Buffer" + i + ":" + j, "");
                    if(i % 6 == 0 && j % 2 == 0) {
                        Directory subSubDir = (Directory)createEntity(controller, subDir,
                                FsEntityType.DIRECTORY,"SubSubDir" + i + ":" + j, data + i);
                        for(int k = 0; k < 25; k++){
                            LogTextFile lg = (LogTextFile)createEntity(controller, subSubDir,
                                    FsEntityType.LOG_TEXT_FILE, "LogFile" + i + ":" + j + ":" + k, data);
                            if (k % 4 == 0 || k % 3 == 0) {
                                controller.submitFSAction(new Move(dir, lg));
                            }
                            if (k % 5 == 0) {
                                controller.submitFSAction(new MoveByString(controller.submitFSAction(new GetPath(dir)).get(), lg));
                            }
                            if (k % 7 == 0) {
                                controller.submitFSAction(new RemoveFromDirectory(subSubDir, lg));
                            }
                        }
                    }
                }
            }
            if(i % 9 == 0){
                String name = "TextFile.txt";
                for (int j = 0; j <= 10; j++) {
                    Directory anotherDir = (Directory)createEntity(controller, dir,
                            FsEntityType.DIRECTORY,"AnotherDir" + j, "");
                    String text = "";
                    if (j % 2 == 0){
                        text += controller.submitFSAction(new Count(dir, true)).get();
                    }
                    if (j % 3 == 0){
                        text += controller.submitFSAction(new Archiving(anotherDir)).get();
                    }
                    if (j % 5 == 0){
                        text += String.join(", ", controller.submitFSAction(new Search(dir, "Sub")).get());
                    }
                    TextFile textFile = (TextFile)createEntity(controller, anotherDir, FsEntityType.TEXT_FILE,name + i + ":" + j, text);
                    if (j % 7 == 0) {
                        controller.submitFSAction(new Move(dir, textFile));
                    }
                }
            }
            if(n != 0 && i % n == 0){
                System.out.println(controller.submitFSAction(new GetPath(dir)).get());
                System.out.println("Files in root: " + controller.submitFSAction(new Count(root, true)).get());
                System.out.println("------------------------------------------------");
            }
        }
        Future<String> futureTree = controller.submitFSAction(new Tree(root));
        System.out.println("__________________________________________________________________________________");
        System.out.println(futureTree.get());
        controller.stop();
    }

    private static FsEntity createEntity(Controller controller, Directory dir, FsEntityType type, String name, String data){
        FsEntity entity = null;
        try{
            Future<FsEntity> future = controller.submitFSAction(new Create(dir, type, name, data));
            entity = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return entity;
    }
}
