package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.FsEntity.FsEntity;

public class MoveByString implements IFsAction<Boolean> {
    private String path;
    private FsEntity entity;

    public MoveByString(String path, FsEntity entity) {
        this.path = path;
        this.entity = entity;
    }
    @Override
    public Boolean execute() {
        try {
            entity.move(path);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
