package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.Directory;
import FileSystem.FsEntity.FsEntity;

public class RemoveFromDirectory implements IFsAction<Boolean> {
    private Directory dir;
    private FsEntity entity;

    public RemoveFromDirectory(Directory dir, FsEntity entity) {
        this.dir = dir;
        this.entity = entity;
    }
    @Override
    public Boolean execute() {
        try{
            dir.removeChild(entity.getName());
            return true;
        } catch (Exception e){
            return false;
        }
    }
}