package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.Directory;
import FileSystem.FsEntity.FsEntity;

import java.util.List;

public class GetFilesInDirectory implements IFsAction<List<FsEntity>> {
    private final Directory directory;

    public GetFilesInDirectory(Directory directory) {
        this.directory = directory;
    }

    @Override
    public List<FsEntity> execute() {
        return directory.getChildren();
    }
}
