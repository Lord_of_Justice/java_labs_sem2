package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.FsEntity.FsEntity;

public class GetPath  implements IFsAction<String> {
    private final FsEntity entity;

    public GetPath(FsEntity entity) {
        this.entity = entity;
    }

    @Override
    public String execute() {
        return entity.getPath();
    }
}