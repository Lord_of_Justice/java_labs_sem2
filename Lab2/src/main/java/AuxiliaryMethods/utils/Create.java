package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.*;
import FileSystem.FsEntity.FsEntity;
import FileSystem.FsEntity.FsEntityType;

public class Create implements IFsAction<FsEntity> {
    private Directory dir;
    private FsEntity entity;
    private FsEntityType type;
    private String name;
    private String data;

    public Create(Directory dir, FsEntityType type, String name, String data) {
        SetDefaultFields(dir, type, name);
        this.data = data;
    }

    private void SetDefaultFields(Directory dir, FsEntityType type, String name){
        this.dir = dir;
        this.type = type;
        this.name = name;
    }
    @Override
    public FsEntity execute() {
        switch (type) {
            case DIRECTORY:
                entity = new Directory(name, dir);
                break;
            case BINARY_FILE:
                entity = new BinaryFile(name, dir, data.getBytes());
                break;
            case BUFFER_FILE:
                entity = new BufferFile(name, dir);
                break;
            case LOG_TEXT_FILE:
                entity = new LogTextFile(name, dir, data);
                break;
            case TEXT_FILE:
                entity = new TextFile(name, dir, data);
                break;
        }
        return entity;
    }
}
