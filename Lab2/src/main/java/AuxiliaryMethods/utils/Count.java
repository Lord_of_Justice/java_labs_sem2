package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.Directory;

public class Count implements IFsAction<Long> {
    private Directory dir;
    private boolean recursive;

    public Count(Directory dir, boolean recursive) {
        this.dir = dir;
        this.recursive = recursive;
    }
    @Override
    public Long execute() {
        return dir.count(recursive);
    }
}
