package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.FsEntity.FsEntity;

public class Delete implements IFsAction<Boolean> {
    private FsEntity entity;

    public Delete(FsEntity entity) {
        this.entity = entity;
    }
    @Override
    public Boolean execute() {
        try {
            entity.delete();
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
