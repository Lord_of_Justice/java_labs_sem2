package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.Directory;

public class Tree implements IFsAction<String> {
    private Directory dir;

    public Tree(Directory dir) {
        this.dir = dir;
    }
    @Override
    public String execute() {
        return dir.tree();
    }
}