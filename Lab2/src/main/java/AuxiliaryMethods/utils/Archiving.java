package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.Directory;

public class Archiving implements IFsAction<String> {
    private Directory dir;

    public Archiving(Directory dir) {
        this.dir = dir;
    }
    @Override
    public String execute() {
        return dir.archiving();
    }
}
