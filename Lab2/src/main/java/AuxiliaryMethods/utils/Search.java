package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.Directory;

import java.util.List;

public class Search implements IFsAction<List<String>> {
    private Directory dir;
    private String pattern;

    public Search(Directory dir, String pattern) {
        this.dir = dir;
        this.pattern = pattern;
    }
    @Override
    public List<String> execute() {
        return dir.search(pattern);
    }
}