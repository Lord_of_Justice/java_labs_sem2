package AuxiliaryMethods.utils;

import AuxiliaryMethods.IFsAction;
import FileSystem.Directory;
import FileSystem.FsEntity.FsEntity;

public class Move implements IFsAction<Boolean> {
    private Directory dir;
    private FsEntity entity;

    public Move(Directory dir, FsEntity entity) {
        this.dir = dir;
        this.entity = entity;
    }
    @Override
    public Boolean execute() {
        try {
            entity.move(dir);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
