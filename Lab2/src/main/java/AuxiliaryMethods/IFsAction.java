package AuxiliaryMethods;

public interface IFsAction<T> {
    T execute();
}
