import FileSystem.FsEntity.FsEntity;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import FileSystem.*;

public class ForkJoinTasksTest {
    Directory root;

    @BeforeEach
    public void init() {
        root = new Directory("root");
        Directory dataDir = new Directory("data", root);
        FsEntity bufferFile  = new BufferFile("bufferFile", dataDir);
        FsEntity binFile = new BinaryFile("binFile", dataDir, "Hello World!".getBytes());
    }

    @Test
    public void search_PartOfString_resultExists() {
        //arrange
        List<String> expectedPath = new ArrayList<>();
        expectedPath.add("/root/data/bufferFile");
        expectedPath.add("/root/data/binFile");

        // act
        List<String> actualPath = root.search("File");

        //assert
        assertTrue(expectedPath.equals(actualPath));
    }

    @Test
    public void count_PartOfString_resultExists() {
        //arrange
        Directory subDir = (Directory)root.getChild("data");

        //act
        Long count = subDir.count("File", true);

        //assert
        assertEquals(2, (long)count);
    }

    @Test
    public void count_ByRecursive_resultExists() {
        //arrange
        Directory subDir = (Directory)root.getChild("data");

        //act
        Long count = subDir.count(true);

        //assert
        assertEquals(2l, (long) count);
    }
}
